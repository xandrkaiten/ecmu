package tests.aboutPageTests;

import Model.AboutPage;
import hellpers.Constants;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tests.BaseTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Story("Проверка страницы О нас")
public class AboutTest extends BaseTest {
    @DisplayName("Переход на Главную страницу")
    @Test
    public void faviconTest() {
        AboutPage aboutPage = new AboutPage(getDriver());
        aboutPage.openAboutPage();
        waitForSecondStep();
        aboutPage.headerFaviconClick();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + "/", aboutPage.getUrl());
    }

    @DisplayName("Переход в пункт выдачи")
    @Test
    public void linkPointsTest() {
        AboutPage aboutPage = new AboutPage(getDriver());
        aboutPage.openAboutPage();
        aboutPage.headerPointsClick();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + "/points", aboutPage.getUrl());
    }

    @DisplayName("Переход в Каталог через блок Изготовление памятников")
    @Test
    public void linkCatalogTest() {
        AboutPage aboutPage = new AboutPage(getDriver());
        aboutPage.openAboutPage();
        scrollToElement(aboutPage.btnCatalogClick());
        waitForSecondStep();
        assertEquals(Constants
                .BASE_PAGE + "/catalog/pamyatniki-na-mogilu", aboutPage.getUrl());
    }

    @DisplayName("Переход в Обслуживание и замена оград")
    @Test
    public void linkFenceTest() throws InterruptedException {
        AboutPage aboutPage = new AboutPage(getDriver());
        aboutPage.openAboutPage();
        scrollToElement(aboutPage.btnServiceFenceClick());
        waitForSecondStep();
        assertEquals(Constants
                .BASE_PAGE + "/services/obsluzhivanie-i-zamena-ograd", aboutPage.getUrl());
    }


    @DisplayName("Переход в Благоустройство захоронения и уход за ним")
    @Test
    public void linkImprovementTest() throws InterruptedException {
        AboutPage aboutPage = new AboutPage(getDriver());
        aboutPage.openAboutPage();
        scrollToElement(aboutPage.btnImprovementClick());
        waitForSecondStep();
        assertEquals(Constants
                .BASE_PAGE + "/services/blagoustroystvo-i-ukhod-za-zakhoroneniem", aboutPage.getUrl());
    }

    @DisplayName("Переход в Обслуживание и ремонт памятников")
    @Test
    public void linkServiceMonumentTest() throws InterruptedException {
        AboutPage aboutPage = new AboutPage(getDriver());
        aboutPage.openAboutPage().btnServiceMonumentClick();
        waitForSecondStep();
        assertEquals(Constants
                .BASE_PAGE + "/services/obsluzhivanie-i-remont-pamyatnikov", aboutPage.getUrl());
    }

    @DisplayName("Открытие аккордеона FAQ")
    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3})
    public void accordionsOpen(int args) {
        AboutPage aboutPage = new AboutPage(getDriver());
        String result = aboutPage.openAboutPage()
                .faqAccordionClick(args).isAccordionExpanded(args);
        waitForSecondStep();
        assertEquals("true", result);
    }

    @DisplayName("Заказ консультации через менеджера с вводом данных")
    @Test
    public void inputName() {
        AboutPage aboutPage = new AboutPage(getDriver());
        aboutPage.openAboutPage()
                .inputNameField().inputPhoneField().checkInputInfo().sendInputInfo().setMakeOrder();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + "/services", aboutPage.getUrl());
    }

    @DisplayName("Переход на страницу Уход за захоронением")
    @Test
    public void linkInvironmentTest() throws InterruptedException {
        AboutPage aboutPage = new AboutPage(getDriver());
        aboutPage.openAboutPage();
        scrollToElement(aboutPage.openInvironment());
        waitForSecondStep();
        assertEquals(Constants
                .BASE_PAGE + "/services/blagoustroystvo-i-ukhod-za-zakhoroneniem", aboutPage.getUrl());
    }
}
