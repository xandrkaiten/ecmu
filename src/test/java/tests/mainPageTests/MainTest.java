package tests.mainPageTests;

import Model.MainPage;
import hellpers.Constants;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tests.BaseTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Story("Проверка главной страницы")
public class MainTest extends BaseTest {
    @DisplayName("Переход на страницу Оплаты")
    @Test
    public void headerLinkBlogTest() {
        MainPage mainPage = new MainPage(getDriver());
        mainPage.openPage().headerPaymentClick();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + "/payment", mainPage.getUrl());
    }

    @DisplayName("Переход в гид по кладбищам")
    @Test
    public void headerLinkGuideTest() {
        MainPage mainPage = new MainPage(getDriver());
        mainPage.openPage().headerGuideClick();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + "/guide", mainPage.getUrl());
    }

    @DisplayName("Переход в магазин")
    @Test
    public void headerLinkShopTest() {
        MainPage mainPage = new MainPage(getDriver());
        mainPage.openPage().btnFindMonumentInCatalogClick();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + "/catalog/pamyatniki-na-mogilu", mainPage.getUrl());
    }
}
