package tests;

import Model.BasePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static java.util.concurrent.TimeUnit.SECONDS;

public abstract class BaseTest {
    protected WebDriver driver;
    protected WebDriverWait wait;

    @BeforeAll
    public static void beforeClass() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    public void beforeTest() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, SECONDS);
        driver.manage().window().maximize();
        wait = new WebDriverWait(getDriver(), Duration.ofSeconds(10));
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }

    protected void scrollToElement(BasePage basePage) {
        basePage.scrollToElement();
    }

    protected void waitForSecondStep() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//footer")));
    }

    protected void waitForPopup() {
        wait.until(ExpectedConditions.elementToBeClickable
                (By.xpath("//*[contains(@class, 'p-dialog p-component p-ripple')]")));
    }

    @AfterEach
    public void close() {
        driver.quit();
    }

    protected WebDriver getDriver() {
        return driver;
    }
}
