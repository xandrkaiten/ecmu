package tests.blogPageTests;

import Model.BlogPage;
import hellpers.Blogs;
import hellpers.Constants;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tests.BaseTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Story("Проверка страницы Блог")
public class BlogTest extends BaseTest {
    @DisplayName("Переход на статьи")
    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3, 4, 5, 6, 7, 8})
    public void linkBlogTest(int ints) {
        BlogPage blogPage = new BlogPage(getDriver());
        blogPage.openBlogPage().clickOnBlogByIndex(ints);
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + Blogs.blogsList.get(ints), blogPage.getUrl());
    }

    @DisplayName("Заказ консультации через менеджера с вводом данных")
    @Test
    public void inputName() {
        BlogPage blogPage = new BlogPage(getDriver());
        blogPage.openBlogPage()
                .inputNameField().inputPhoneField().checkInputInfo().sendInputInfo().setMakeOrder();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + "/services", blogPage.getUrl());
    }

    @DisplayName("Переход на страницу Уход за захоронением")
    @Test
    public void linkInvironmentTest() throws InterruptedException {
        BlogPage blogPage = new BlogPage(getDriver());
        blogPage.openBlogPage();
        scrollToElement(blogPage.openInvironment());
        waitForSecondStep();
        assertEquals(Constants
                .BASE_PAGE + "/services/blagoustroystvo-i-ukhod-za-zakhoroneniem", blogPage.getUrl());
    }
}