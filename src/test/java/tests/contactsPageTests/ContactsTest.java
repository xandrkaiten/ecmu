package tests.contactsPageTests;

import Model.ContactsPage;
import hellpers.Constants;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tests.BaseTest;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Story("Проверка страницы Контакты")
public class ContactsTest extends BaseTest {
    @DisplayName("Переход по внешней ссылке YandexMap")
    @Test
    public void FindUs() {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage().btnYandexMapClick();
        Set<String> windowHandles = getDriver().getWindowHandles();
        for (String windowHandle : windowHandles) {
            if (!windowHandle.equals(getDriver().getWindowHandle())) {
                getDriver().switchTo().window(windowHandle);
                break;
            }
        }
        assertTrue(contactsPage.getUrl().contains("yandex.ru"));
    }

    @DisplayName("Переход по внешней ссылке Telegram")
    @Test
    public void linkTelegram() {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage().btnTelegramClick();
        Set<String> windowHandles = getDriver().getWindowHandles();
        for (String windowHandle : windowHandles) {
            if (!windowHandle.equals(getDriver().getWindowHandle())) {
                getDriver().switchTo().window(windowHandle);
                break;
            }
        }
        assertTrue(contactsPage.getUrl().contains("https://web.telegram.org/"));
    }

    @DisplayName("Переход по внешней ссылке WhatsApp")
    @Test
    public void linkWhatsApp() {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage().btnWhatsAppClick();
        Set<String> windowHandles = getDriver().getWindowHandles();
        for (String windowHandle : windowHandles) {
            if (!windowHandle.equals(getDriver().getWindowHandle())) {
                getDriver().switchTo().window(windowHandle);
                break;
            }
        }
        assertTrue(contactsPage.getUrl().contains("https://www.whatsapp.com/"));
    }

    @DisplayName("Открытие аккордеона FAQ")
    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3})
    public void accordionsOpen(int args) {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        String result = contactsPage
                .openContactsPage().faqAccordionClick(args).isAccordionExpanded(args);
        waitForSecondStep();
        assertEquals("true", result);
    }

    @DisplayName("Отправить запрос менеджеру через Popup")
    @Test
    public void callManagerAndPopup() {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage().btnCallManagerClick();
        waitForPopup();
        contactsPage.inputNameFieldPopup().inputPhoneFieldPopup().inputProblemPopup();
        contactsPage.btnSendPopupClick();
        assertEquals("Спасибо за вашу заявку!", contactsPage.getPopupTextOrder());
    }

    @DisplayName("Заказ консультации через менеджера с вводом данных")
    @Test
    public void inputDataAndOrder() {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage()
                .inputNameField().inputPhoneField()
                .checkInputInfo().sendInputInfo().setMakeOrder();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + "/services", contactsPage.getUrl());
    }

    @DisplayName("Вывод ошибки при вводе некорректных данных в поле Телефон")
    @Test
    public void inputWrongPhone() {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage().inputNameField().inputWrongPhoneField();
        waitForSecondStep();
        assertTrue(contactsPage.errorPhoneNumber());
    }

    @DisplayName("Вывод ошибки при отсутствии данных в поле Имя")
    @Test
    public void inputEmptyName() {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage().checkInputInfo().inputPhoneField().sendInputInfo();
        waitForSecondStep();
        assertTrue(contactsPage.errorEmptyName());
    }

    @DisplayName("Заказ консультации через менеджера с вводом данных до появления Popup")
    @Test
    public void inputDataCheckPopup() {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage()
                .inputNameField().inputPhoneField()
                .checkInputInfo().sendInputInfo();
        waitForPopup();
        assertEquals("Спасибо за вашу заявку!", contactsPage.getPopupText());
    }

    @DisplayName("Вывод ошибки при вводе некорректых данных в поле Телефон Popup")
    @Test
    public void invalidInputNameCheckError() {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage().btnCallManagerClick();
        waitForPopup();
        contactsPage.inputInvalidPhoneFieldPopup().btnSendPopupClick();
        assertTrue(contactsPage.isErrorPresent());
    }

    @DisplayName("Переход на страницу Уход за захоронением")
    @Test
    public void linkInvironmentTest() throws InterruptedException {
        ContactsPage contactsPage = new ContactsPage(getDriver());
        contactsPage.openContactsPage().openInvironment().getUrl();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE
                + "/services/blagoustroystvo-i-ukhod-za-zakhoroneniem", contactsPage.getUrl());
    }
}
