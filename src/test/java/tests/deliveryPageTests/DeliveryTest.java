package tests.deliveryPageTests;

import Model.DeliveryPage;
import hellpers.Constants;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tests.BaseTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Story("Проверка страницы Доставка")
public class DeliveryTest extends BaseTest {

    @DisplayName("Открытие динамического блока Пункты выдачи с картой")
    @Test
    public void checkDynamicObjPoints() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage().openDynamicPointsClick();
        waitForSecondStep();
        assertEquals("Забрать заказ самостоятельно можно из пунктов выдачи",
                deliveryPage.getOpenDynamicPoints());
    }

    @DisplayName("Переход на страницу Points через Подробнее Динамического блока")
    @Test
    public void goToPointsThrDynamic() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage().openDynamicPointsClick();
        waitForSecondStep();
        deliveryPage.goToPointsClick();
        assertEquals(Constants.BASE_PAGE + "/", deliveryPage.getUrl());
    }

    @DisplayName("Открытие динамического блока Служба доставки с таблицей")
    @Test
    public void checkDynamicObjDeliverySrv() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage();
        deliveryPage.openDynamicDeliverySrvClick();
        waitForSecondStep();
        assertEquals("Стоимость доставки до кладбища",
                deliveryPage.getOpenDynamicDeliverySrv());
    }

    @DisplayName("Поиск по городу")
    @Test
    public void checkFilterCity() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage().openDynamicDeliverySrvClick()
                .inputSearchCemeteryField("Москва").getResultColumn("Город");
        waitForSecondStep();
        assertTrue(deliveryPage.getResultColumn("Город")
                .stream().allMatch(s -> s.contains("Москва")));
    }

    @DisplayName("Поиск по названию кладбища")
    @Test
    public void checkFilterCemetery() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage().openDynamicDeliverySrvClick();
        deliveryPage.inputSearchCemeteryField("Ваганьковское")
                .getResultColumn("Название кладбища");
        waitForSecondStep();
        assertTrue(deliveryPage.getResultColumn("Название кладбища")
                .stream().allMatch(s -> s.contains("Ваганьковское")));
    }

    @DisplayName("Открытие аккордеона FAQ")
    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2, 3})
    public void accordionsOpen(int args) {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        String result = deliveryPage.openDeliveryPage()
                .faqAccordionClick(args).isAccordionExpanded(args);
        waitForSecondStep();
        assertEquals("true", result);
    }

    @DisplayName("Отправить запрос менеджеру через Popup")
    @Test
    public void callManagerAndPopup() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage().btnCallManagerClick();
        waitForPopup();
        deliveryPage.inputNameFieldPopup().inputPhoneFieldPopup().inputProblemPopup();
        deliveryPage.btnSendPopupClick();
        assertEquals("Спасибо за вашу заявку!", deliveryPage.getPopupTextOrder());
    }

    @DisplayName("Заказ консультации через менеджера с вводом данных")
    @Test
    public void inputDataAndOrder() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage()
                .inputNameField().inputPhoneField()
                .checkInputInfo().sendInputInfo();
        waitForPopup();
        deliveryPage.setMakeOrder();
        waitForSecondStep();
        assertEquals(Constants.BASE_PAGE + "/services", deliveryPage.getUrl());
    }

    @DisplayName("Вывод ошибки при вводе некорректных данных в поле Телефон")
    @Test
    public void inputWrongPhone() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage().inputNameField().inputWrongPhoneField();
        waitForSecondStep();
        assertTrue(deliveryPage.errorPhoneNumber());
    }

    @DisplayName("Вывод ошибки при вводе некорректных данных в поле Имя")
    @Test
    public void inputEmptyName() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage().checkInputInfo().inputPhoneField().sendInputInfo();
        waitForSecondStep();
        assertTrue(deliveryPage.errorEmptyName());
    }

    @DisplayName("Заказ консультации через менеджера с вводом данных до появления Popup")
    @Test
    public void inputDataCheckPopup() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage()
                .inputNameField().inputPhoneField()
                .checkInputInfo().sendInputInfo();
        waitForPopup();
        assertEquals("Спасибо за вашу заявку!", deliveryPage.getPopupText());
    }

    @DisplayName("Вывод ошибки при вводе некорректых данных в поле Телефон Popup")
    @Test
    public void invalidInputNameCheckError() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage().btnCallManagerClick()
                .inputInvalidPhoneFieldPopup().btnSendPopupClick();
        assertTrue(deliveryPage.isErrorPresent());
    }

    @DisplayName("Переход на страницу Уход за захоронением")
    @Test
    public void linkInvironmentTest() {
        DeliveryPage deliveryPage = new DeliveryPage(getDriver());
        deliveryPage.openDeliveryPage().openInvironment().getUrl();
        waitForSecondStep();
        assertEquals(Constants
                        .BASE_PAGE + "/services/blagoustroystvo-i-ukhod-za-zakhoroneniem",
                deliveryPage.getUrl());
    }
}
