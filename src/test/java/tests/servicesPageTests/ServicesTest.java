package tests.servicesPageTests;

import Model.ServicesPage;
import hellpers.Constants;
import io.qameta.allure.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tests.BaseTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Story("Проверка страницы Услуги")
public class ServicesTest extends BaseTest {
    @DisplayName("Переход на страницу Уход за захоронением")
    @Test
    public void goToBurialCareOrder() throws InterruptedException {
        ServicesPage servicesPage = new ServicesPage(getDriver());
        servicesPage.openServicesPage().openInvironment();
        waitForSecondStep();
        assertEquals(Constants
                .BASE_PAGE + "/services/blagoustroystvo-i-ukhod-za-zakhoroneniem", servicesPage.getUrl());
    }

    @DisplayName("Переход на страницу Обслуживание и замена оград")
    @Test
    public void goToRepairAndReplacementFenceOrder() throws InterruptedException {
        ServicesPage servicesPage = new ServicesPage(getDriver());
        servicesPage.openServicesPage().goToReplacementFenceOrder();
        waitForSecondStep();
        assertEquals(Constants
                .BASE_PAGE + "/services/obsluzhivanie-i-zamena-ograd", servicesPage.getUrl());
    }
}
