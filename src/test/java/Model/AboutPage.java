package Model;

import hellpers.Constants;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class AboutPage extends BasePage {

    public AboutPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[text()='Смотреть каталог']\n")
    private WebElement btnCatalog;

    @FindBy(xpath = "//h4[contains(text(), 'замена оград')]/../following-sibling::a")
    private WebElement btnKnowMoreServiceFence;

    @FindBy(xpath = "//h4[contains(text(), 'Благоустройство')]/../following-sibling::a")
    private WebElement btnKnowMoreImprovement;

    @FindBy(xpath = "//h4[contains(text(), 'ремонт памятников')]/../following-sibling::a")
    private WebElement btnKnowMoreServiceMonument;

    @Step("Открытие страницу О нас")
    public AboutPage openAboutPage() {
        getDriver().get(Constants.BASE_PAGE + "/about");
        return this;
    }

    @Step("Открытие страницы Изготовление памятников")
    public AboutPage btnCatalogClick() {
        this.btnCatalog.click();
        return new AboutPage(getDriver());
    }

    @Step("Открытие страницы Обслуживание и замена оград")
    public AboutPage btnServiceFenceClick() throws InterruptedException {
        jsClick(btnKnowMoreServiceFence);
        return new AboutPage(getDriver());
    }

    @Step("Открытие страницы Благоустройство захоронения и уход за ним")
    public AboutPage btnImprovementClick() throws InterruptedException {
        jsClick(btnKnowMoreImprovement);
        return new AboutPage(getDriver());
    }

    @Step("Открытие страницы Обслуживание и ремонт памятников")
    public AboutPage btnServiceMonumentClick() throws InterruptedException {
        jsClick(btnKnowMoreServiceMonument);
        return new AboutPage(getDriver());
    }
}
