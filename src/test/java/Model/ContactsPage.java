package Model;

import hellpers.Constants;
import io.qameta.allure.Step;
import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ContactsPage extends BasePage {
    public ContactsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[text()=\"Как проехать?\"]\n")
    private WebElement btnYandexMap;

    @FindBy(xpath = "//a[@href=\"tel:78005553535\"]")
    private WebElement btnPhoneNumber;

    @FindBy(xpath = "//a[@href=\"mailto:info@ecmu.ru\"]")
    private WebElement btnEmail;

    @FindBy(xpath = "//a[@href=\"https://web.telegram.org/k/\"]")
    private WebElement btnTelegram;

    @FindBy(xpath = "//a[@href=\"https://www.whatsapp.com/\"]")
    private WebElement btnWhatsApp;

    @FindBy(xpath = "//a[text()=\"Как проехать?\"]\n")
    private WebElement yandexMap11;

    @FindBy(xpath = "//a[text()=\"Как проехать?\"]\n")
    private WebElement yandexMap1;

    @Step("Открытие страницы Контакты")
    public ContactsPage openContactsPage() {
        getDriver().get(Constants.BASE_PAGE + "/contacts");
        return this;
    }

    @Step("Переход по ссылке Яндекс")
    public ContactsPage btnYandexMapClick() {
        this.btnYandexMap.click();
        return this;
    }

    @Step("Клик по номеру телефона")
    public ContactsPage btnPhoneNumberClick() {
        this.btnPhoneNumber.click();
        return this;
    }

    @Step("Клик по электронной почте")
    public ContactsPage btnEmailClick() {
        this.btnEmail.click();
        return this;
    }

    @Step("Переход по ссылке Telegram")
    public ContactsPage btnTelegramClick() {
        this.btnTelegram.click();
        return this;
    }

    @Step("Переход по ссылке WhatsApp")
    public ContactsPage btnWhatsAppClick() {
        this.btnWhatsApp.click();
        return this;
    }
}
