package Model;

import hellpers.Constants;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class BlogPage extends BasePage {
    public BlogPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div [contains(@class, \"gap-[140px]\")]//a")
    List<WebElement> newBlogs;

    @Step("Переход на страницу блога")
    public void clickOnBlogByIndex(int index) {
        newBlogs.get(index).click();
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(5));
        wait.until(ExpectedConditions.urlContains("/blog/"));
    }

    @Step("Открытие страницы Блог")
    public BlogPage openBlogPage() {
        getDriver().get(Constants.BASE_PAGE + "/blog");
        return this;
    }
}
