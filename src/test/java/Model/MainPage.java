package Model;

import hellpers.Constants;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BasePage {
    public MainPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(linkText = "Выбрать памятник")
    private WebElement btnSelectMonument;

    @FindBy(xpath = "//span[text() = 'Найти памятник в каталоге']")
    private WebElement btnFindMonumentInCatalog;

    @FindBy(xpath = "//span[text() = 'Выбрать все необходимые услуги']")
    private WebElement btnSelectServices;

    @FindBy(xpath = "//span[text() = 'Оплатить памятник в нашем магазине']")
    private WebElement btnPay;

    @FindBy(xpath = "//span[text() = 'Получить его в пункте выдачи']")
    private WebElement btnPoints;

    @Step("Открыть главную страницу")
    public MainPage openPage() {
        getDriver().get(Constants.BASE_PAGE);
        return this;
    }

    @Step("Открытие страницы памятник")
    public ShopPage btnSelectMonumentClick() {
        this.btnSelectMonument.click();
        return new ShopPage(getDriver());
    }

    @Step("Открытие страницы каталог")
    public ShopPage btnFindMonumentInCatalogClick() {
        this.btnFindMonumentInCatalog.click();
        return new ShopPage(getDriver());
    }

    @Step("Открытие страницы выбор услуг")
    public ServicesPage btnSelectServicesClick() {
        this.btnSelectServices.click();
        return new ServicesPage(getDriver());
    }

    @Step("Открытие страницы оплата")
    public PaymentPage btnPayClick() {
        this.btnPay.click();
        return new PaymentPage(getDriver());
    }

    @Step("Открытие страницы Получить его в пункте выдачи")
    public ShopPage btnPointsClick() {
        this.btnPoints.click();
        return new ShopPage(getDriver());
    }
}