package Model;

import hellpers.Constants;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ServicesPage extends BasePage {
    public ServicesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h2[contains(text(), 'уход')]/following-sibling::a[text()=\"Заказать услугу\"]")
    private WebElement burialCareOrder;

    @FindBy(xpath = "//h2[contains(text(), 'замена оград')]/following-sibling::a[text()=\"Заказать услугу\"]")
    private WebElement replacementFenceOrder;

    @Step("Переход на страницу Обслуживание и замена оград")
    public ServicesPage goToReplacementFenceOrder() throws InterruptedException {
        jsClick(replacementFenceOrder);
        return this;
    }

    @Step("Переход на страницу Уход за захоронением")
    public ServicesPage goToBurialCareOrder() throws InterruptedException {
        jsClick(burialCareOrder);
        return this;
    }

    @Step("Открытие страницы Доставка")
    public ServicesPage openServicesPage() {
        getDriver().get(Constants.BASE_PAGE + "/services");
        return this;
    }
}