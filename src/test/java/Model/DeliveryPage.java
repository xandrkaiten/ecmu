package Model;

import hellpers.Constants;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DeliveryPage extends BasePage {
    public DeliveryPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h4[contains(text(), 'Пункты выдачи')]")
    private WebElement dynamicPoints;

    @FindBy(xpath = "//h3[contains(text(), 'Забрать заказ самостоятельно')]")
    private WebElement openedDynamicPoints;

    @FindBy(xpath = "//a[text()='Подробнее']")
    private WebElement goToPoints;

    @FindBy(xpath = "//h4[contains(text(), 'Служба доставки')]")
    private WebElement dynamicDeliverySrv;

    @FindBy(xpath = "//h3[contains(text(), 'Стоимость доставки до кладбища')]")
    private WebElement openedDynamicDeliverySrv;

    @FindBy(xpath = "//input[@placeholder='Введите название кладбища, город, населенный пункт']")
    private WebElement inputSearchCemetery;

    @FindBy(xpath = "//table/tr/td[1]")
    List<WebElement> columnCity;

    @FindBy(xpath = "//table/tr/td[2]")
    List<WebElement> columnCemetery;

    public List<String> getResultColumn(String columnName) {
        if (Objects.equals(columnName, "Город")) {
            return columnCity.stream().map(WebElement::getText).collect(Collectors.toList());
        } else if (Objects.equals(columnName, "Название кладбища")) {
            return columnCemetery.stream().map(WebElement::getText).collect(Collectors.toList());
        } else {
            return null;
        }
    }

    @Step("Ввод данных в поле Имя Вызов менеджера")
    public DeliveryPage inputSearchCemeteryField(String text) {
        this.inputSearchCemetery.sendKeys(text);
        return this;
    }

    @Step("Переход через (подробнее) в Пункты выдачи")
    public DeliveryPage goToPointsClick() {
        this.goToPoints.click();
        return this;
    }

    @Step("Открытие динамического блока Пункты выдачи")
    public DeliveryPage openDynamicPointsClick() {
        this.dynamicPoints.click();
        return this;
    }

    @Step("Текст из блока Пункты выдачи")
    public String getOpenDynamicPoints() {
        return openedDynamicPoints.getText();
    }

    @Step("Открытие динамического блока Служба доставки")
    public DeliveryPage openDynamicDeliverySrvClick() {
        this.dynamicDeliverySrv.click();
        return this;
    }

    @Step("Текст из блока Служба доставки")
    public String getOpenDynamicDeliverySrv() {
        return openedDynamicDeliverySrv.getText();
    }

    @Step("Открытие страницы Доставка")
    public DeliveryPage openDeliveryPage() {
        getDriver().get(Constants.BASE_PAGE + "/delivery");
        return this;
    }
}
