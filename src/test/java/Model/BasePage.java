package Model;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class BasePage {

    private final WebDriver driver;

    public WebDriver getDriver() {
        return driver;
    }

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(getDriver(), this);
    }

    @FindBy(xpath = "//a[@href='/' and contains(@class, 'mobile:w-20')]")
    private WebElement headerFavicon;

    @FindBy(xpath = "//header//a[@href=\"/guide\"]")
    private WebElement headerGuide;

    @FindBy(xpath = "//header//a[@href=\"/payment\"]")
    private WebElement headerPayment;

    @FindBy(xpath = "//header//a[@href=\"/points\"]")
    private WebElement headerPoints;

    @FindBy(css = "input[placeholder='Имя']\n")
    private WebElement inputName;

    @FindBy(css = "input[name=\"phone\"]")
    private WebElement inputPhone;

    @FindBy(xpath = "//label[@for=\"checked\"]\n")
    private WebElement checkInput;

    @FindBy(xpath = "//span[contains (text(), 'Получить консультацию')]\n")
    private WebElement sendInputInfo;

    @FindBy(xpath = "//button[normalize-space() = 'Отправить']\n")
    private WebElement sendInputInfoPopup;

    @FindBy(xpath = "//a[contains(text(), 'Заказать услугу')]\n")
    private WebElement makeOrder;

    @FindBy(xpath = "//a[text()=\"Перейти\"]")
    private WebElement goToInvironment;

    @FindBy(xpath = "//img[@src='/svg/logo.svg']")
    private WebElement footFavicon;

    @FindBy(xpath = "//div[@data-pc-name='accordiontab']")
    private List<WebElement> faqAccordions;

    @FindBy(xpath = "//span[contains(@class," +
            " 'text-red300 text-12') and contains(text(), 'Некорректный номер телефона')]\n")
    private WebElement errorPhoneIncorrect;

    @FindBy(xpath = "//h3[contains(text()," +
            " 'Связаться с менеджером')]/following::span[contains(text(), 'Некорректный номер телефона')]")
    private WebElement errorPhoneIncorrectPopup;

    @FindBy(xpath = "//span[contains(@class," +
            " 'text-red300 text-12') and contains(text(), 'Введите Ваше имя')]\n")
    private WebElement errorInputName;

    @FindBy(css = "h3.text-64.tracking-popup")
    private WebElement popupText;

    @FindBy(xpath = "//h3[contains(text(), 'Спасибо за вашу заявку!')]")
    private WebElement popupTextOrder;

    @FindBy(xpath = "//h3[contains(text(), 'Связаться с менеджером')]/following::input[@placeholder='Имя']")
    private WebElement nameFieldPopup;

    @FindBy(xpath = "//h3[contains(text(), 'Связаться с менеджером')]" +
            "/following::input[@placeholder='+7 (. )']")
    private WebElement phoneFieldPopup;

    @FindBy(xpath = "//h3[contains(text(), 'Связаться с менеджером')]" +
            "/following::textarea[@placeholder='Опишите проблему']")
    private WebElement problemFieldPopup;

    @FindBy(xpath = "//button[normalize-space()=\"Связаться с менеджером\"]")
    private WebElement btnCallManager;

    @FindBy(xpath = "//button[normalize-space()=\"Отправить\"]")
    private WebElement btnSendPopup;

    @Step("Подтверждение отправки данных в Popup")
    public BasePage btnSendPopupClick() {
        this.btnSendPopup.click();
        return this;
    }

    @Step("Ввод данных а поле Опишите проблему Popup")
    public BasePage inputProblemPopup() {
        this.problemFieldPopup.sendKeys("Problem");
        return this;
    }

    @Step("Вызов Popup Связаться с менеджером")
    public BasePage btnCallManagerClick() {
        this.btnCallManager.click();
        return this;
    }

    @Step("Ввод номера телефона Popup")
    public BasePage inputPhoneFieldPopup() {
        this.phoneFieldPopup.sendKeys("89378816867");
        return this;
    }

    @Step("Ввод имени Popup")
    public BasePage inputNameFieldPopup() {
        this.nameFieldPopup.sendKeys("PopupName");
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable
                (By.xpath("//button[normalize-space()=\"Отправить\"]")));
        return this;
    }

    @Step("Ввод букв вместо номера телефона Popup")
    public BasePage inputInvalidPhoneFieldPopup() {
        this.phoneFieldPopup.sendKeys("Must be an error");
        return this;
    }

    @Step("Текст из Popup")
    public String getPopupText() {
        return popupText.getText();
    }

    @Step("Текст из Popup")
    public String getPopupTextOrder() {
        return popupTextOrder.getText();
    }

    @Step("Вывод ошибки некорректный телефон")
    public boolean errorPhoneNumber() {
        this.errorPhoneIncorrect.isDisplayed();
        return true;
    }

    @Step("Вывод ошибки некорректный телефон Popup")
    public boolean isErrorPresent() {
        return errorPhoneIncorrectPopup.isDisplayed();
    }

    @Step("Вывод ошибки Введите ваше имя")
    public boolean errorEmptyName() {
        this.errorInputName.isDisplayed();
        return true;
    }

    @Step("Открытие главной страницы")
    public MainPage headerFaviconClick() {
        this.headerFavicon.click();
        return new MainPage(getDriver());
    }

    @Step("Открытие страницы Гид по кладбищам")
    public GuidePage headerGuideClick() {
        this.headerGuide.click();
        return new GuidePage(getDriver());
    }

    @Step("Открытие страницы Оплата")
    public PaymentPage headerPaymentClick() {
        this.headerPayment.click();
        return new PaymentPage(getDriver());
    }

    @Step("Открытие страницы Пункты выдачи")
    public PointsPage headerPointsClick() {
        this.headerPoints.click();
        return new PointsPage(getDriver());
    }

    @Step("Ввод данных в поле Имя Вызов менеджера")
    public BasePage inputNameField() {
        this.inputName.sendKeys("Федор");
        return this;
    }

    @Step("Ввод данных в поле телефон Вызов менеджера")
    public BasePage inputPhoneField() {
        this.inputPhone.sendKeys("79125436354");
        return this;
    }

    @Step("Ввод некорректных данных в поле телефон Вызов менеджера")
    public BasePage inputWrongPhoneField() {
        this.inputPhone.sendKeys("Error");
        return this;
    }

    @Step("Согласие на обработку персональных данных Вызов менеджера")
    public BasePage checkInputInfo() {
        this.checkInput.click();
        return this;
    }

    @Step("Заказать услугу в попап окне Спасибо за вашу заявку")
    public BasePage setMakeOrder() {
        this.makeOrder.click();
        return this;
    }

    @Step("Отправить данные, получить консультацию")
    public BasePage sendInputInfo() {
        this.sendInputInfo.click();
        return this;
    }

    @Step("Отправить данные, получить консультацию")
    public BasePage sendInputInfoPopup() {
        this.sendInputInfoPopup.click();
        return this;
    }

    @Step("Открытие страницы Благоустройство памятников")
    public BasePage openInvironment() {
        jsClick(goToInvironment);
        return this;
    }

    @Step("Раскрытие акордеона в Частые вопросы")
    public BasePage faqAccordionClick(int number) {
        this.faqAccordions.get(number).click();
        return this;
    }

    @Step("Проверка раскрытия аккордеона")
    public String isAccordionExpanded(int number) {
        return faqAccordions.get(number).getAttribute("data-p-active");
    }

    @Step("Получение URL")
    public String getUrl() {
        return getDriver().getCurrentUrl();
    }

    @Step("Клик с ожиданием прогрузки страницы")
    public void jsClick(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", element);
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//footer")));
    }

    public void scrollToElement() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.scrollBy(0,250)", "50000");
    }
}